import axios from "axios"
export const baseURL = "https://fakestoreapi.com/"
export const baseAPI = axios.create({
  baseURL,
  timeout: 1000,
 
});