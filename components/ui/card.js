import Card from "@mui/material/Card";
import CardHeader from "@mui/material/CardHeader";
import CardMedia from "@mui/material/CardMedia";
import CardContent from "@mui/material/CardContent";
import Typography from "@mui/material/Typography";
import Image from "next/image";
import {
  Icon,
  Rating,
  Chip,
  Grid,
  CardActionArea,
  Button,
} from "@mui/material";
import { StarRate, RemoveRedEye } from "@mui/icons-material";
import { useRouter } from "next/router";

function ProductCard(props) {
  const {id, title, price, description, category, image, rating:  { rate, count } } = props;
   const router = useRouter();
  return (
    <Card sx={{ maxWidth: 320, margin: 2 }} key={id}>
      <CardActionArea
        onClick={() => {
          router.push(`/products/${id}`);
        }}
      >
        <CardHeader
          title={title}
          subheader={category}
          component="h4"
        />
        <CardMedia height="194" alt={title}>
          <Image
            src={image}
            alt="id"
            key={id}
            width={340}
            height={300}
          />
        </CardMedia>
        <CardContent key={`1-${id}`}>
          <Typography variant="body2" color="text.secondary">
            {description}
          </Typography>
        </CardContent>

        <CardContent disableSpacing key={`2-${id}`}>
          <Grid container>
            <Grid item xs={5}>
              <Rating name="read-only" value={rate} readOnly />
            </Grid>
            <Grid item xs={4}>
              <Chip label={`Cost ${price} $`} color="primary" />
            </Grid>
            <Grid item xs={3}>
              <Chip icon={<RemoveRedEye />} label={count} />
            </Grid>
          </Grid>
        </CardContent>
      </CardActionArea>
    </Card>
  );
}

export default ProductCard;
