// import Link from 'next/link'
// import classes from './main-header.module.css'
import {Box, AppBar, Toolbar, Typography, Button} from '@mui/material'
import { useRouter } from "next/router";
function MainHeader() {
    const router = useRouter();

return (
  <Box sx={{ flexGrow: 1 }}>
    <AppBar position="static">
      <Toolbar>
        <Button
          color="inherit"
          onClick={() => {
            router.push("/products");
          }}
        >
          <Typography variant="h6" component="div" sx={{ flexGrow: 1 }}>
            Home
          </Typography>
        </Button>
      </Toolbar>

     
    </AppBar>
  </Box>
);
}
export default MainHeader