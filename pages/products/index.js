
import { useRouter } from "next/router";
import {baseAPI} from '../../config'
import Card from '../../components/ui/card'
import {Box, Item} from '@mui/material'

function AllProducts(props) {

  const route = useRouter();

  const { products, categories } = props;


  return (
    <>
      {/* <p>{JSON.stringify(categories)}</p>
      <button onClick={()=>{route.push({
        pathname: `products`,
        query: { categories: "jewelery" },
      });}}>jewelery</button> */}

      <Box
        sx={{
          display: "flex",
          flexWrap: "wrap",
          flexDirection: "row",
        }}
      >
        {products.map((val) => (
          <Card {...val} key={val.id} />
        ))}
      </Box>
    </>
  );
}


export async function getStaticProps(context) {
    const response = await baseAPI.get("products");
    const categories = await baseAPI.get("products/categories");
    return {
      props: {
        products: response.data,
        categories: categories.data,
      },
    };
}



export default AllProducts;
