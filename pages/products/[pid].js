
import { baseAPI } from "../../config";
import {
  Grid,
  Card,
  CardHeader,
  Typography,
  CardContent,
  Chip,
  Rating,
} from "@mui/material";
import { RemoveRedEye , } from "@mui/icons-material";
import Image from "next/image";

function ProductDetails(props) {
//   const router = useRouter();
//   const productId = router.query.productId;
  const { loadedProduct } = props;
//   const event = getEventById(eventId);
//   if (!event) {
//     return <p>No Event found!</p>;
//   }
if (!loadedProduct){
  return (<>...Loading</>)
}
  return (
    <Grid container spacing={2}>
      <Grid item xs={4}>
        <div style={{ margin: 12 }}>
          <Image
            src={loadedProduct.image}
            alt="loadedProduct.id"
            width={340}
            height={300}
            key={loadedProduct.id}
          />
        </div>
      </Grid>
      <Grid item xs={8}>
        <Card sx={{ margin: 2 }} elevation={0}>
          <CardHeader
            title={loadedProduct.title}
            subheader={loadedProduct.category}
            component="h4"
          />
          <CardContent>
            <Typography variant="body2" color="text.secondary">
              {loadedProduct.description}
            </Typography>
          </CardContent>

          <CardContent disableSpacing>
            <Grid container>
              <Grid item xs={2}>
                <Rating
                  name="read-only"
                  value={loadedProduct.rating.rate}
                  readOnly
                />
              </Grid>
              <Grid item xs={2}>
                <Chip
                  label={`Price ${loadedProduct.price} $`}
                  color="primary"
                />
              </Grid>
              <Grid item xs={4}>
                <Chip
                  icon={<RemoveRedEye />}
                  label={loadedProduct.rating.count}
                />
              </Grid>
            </Grid>
          </CardContent>
        </Card>
      </Grid>
    </Grid>
  );
}


export async function getStaticProps(context) {
  const { params } = context;
  const productId = params.pid;
    const response = await baseAPI.get(`products/${productId}`);
  return {
    props: {
      loadedProduct: response.data,
    },
  };
}


export async function getStaticPaths(context) {

const {params} = context
  return {
      paths: [{params : {pid : "1"}}],
      fallback :  true
  };
}


export default ProductDetails;
