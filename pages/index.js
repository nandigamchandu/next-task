import { useRouter } from "next/router";
import {baseAPI} from '../config'
import Card from '../components/ui/card'
import {Box, Item} from '@mui/material'

function HomePage(props) {

  const route = useRouter();

  const { products, categories } = props;


  return (
    <>
      <Box
        sx={{
          display: "flex",
          flexWrap: "wrap",
          flexDirection: "row",
        }}
      >
        {products.map((val) => (
          <Card {...val} key={val.id} />
        ))}
      </Box>
    </>
  );
}


export async function getStaticProps(context) {
    const response = await baseAPI.get("products");
    const categories = await baseAPI.get("products/categories");
    return {
      props: {
        products: response.data,
        categories: categories.data,
      },
    };
}



export default HomePage;
